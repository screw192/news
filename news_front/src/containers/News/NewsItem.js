import React from 'react';
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import moment from "moment";


import {apiURL} from "../../config";
import axiosApi from "../../axiosApi";
import {fetchNews} from "../../store/actions/newsActions";
import imageNotAvailable from "../../assets/images/NotAvailable.jpg";


const useStyles = makeStyles({
  paper: {
    padding: "15px"
  },
  img: {
    display: "block",
    marginRight: "20px",
    borderRadius: "5px"
  },
  title: {
    fontSize: "18px",
    fontWeight: "bold"
  },
  timestamp: {
    color: "#777777",
    marginRight: "10px"
  }
});

const NewsItem = ({id, image, title, timestamp}) => {
  const classes = useStyles();

  const dispatch = useDispatch();

  let cardImage;
  if (image) {
    cardImage = apiURL + "/uploads/" + image;
  } else {
    cardImage = imageNotAvailable;
  }

  const datetime = moment(timestamp).format("DD/MM/YY hh:mm:ss");

  const onClickHandler = async id => {
    await axiosApi.delete("/news/" + id);
    dispatch(fetchNews());
  };

  return (
      <Grid item>
        <Paper className={classes.paper} variant="outlined">
          <Grid item container alignItems="center">
            <Grid item>
              <img className={classes.img} src={cardImage} alt="" height="150px" width="auto"/>
            </Grid>
            <Grid item xs container direction="column">
              <Grid item>
                <Typography
                    className={classes.title}
                    variant="body1"
                >
                  {title}
                </Typography>
              </Grid>
              <Grid item>
                <Typography
                    className={classes.timestamp}
                    display="inline"
                    variant="body2"
                >
                  {datetime}
                </Typography>
                <Link to={"/news/" + id}>Read full post>></Link>
              </Grid>
            </Grid>
            <Grid item>
              <Button
                  color="secondary"
                  variant="contained"
                  onClick={() => onClickHandler(id)}
              >
                Delete
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
  );
};

export default NewsItem;