import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";


import NewsItem from "./NewsItem";
import {fetchNews} from "../../store/actions/newsActions";
import {initArticleComments} from "../../store/actions/articleActions";


const useStyles = makeStyles({
  link: {
    color: "#ffffff",
    textDecoration: "none"
  }
});

const News = () => {
  const classes = useStyles();

  const dispatch = useDispatch();
  const newsArray = useSelector(state => state.news.news);

  useEffect(() => {
    dispatch(fetchNews());
    dispatch(initArticleComments());
  }, [dispatch]);

  const newsBlock = newsArray.map(item => {
    return (
        <NewsItem
            key={"newsID" + item.id}
            id={item.id}
            image={item.image}
            title={item.title}
            timestamp={item.datetime}
        />
    );
  });

  return (
      <Grid container direction="column" spacing={2}>
        <Grid item container justify="space-between" alignItems="center">
          <Typography variant="h4">Новости</Typography>
          <Link
              className={classes.link}
              to="/add_post"
          >
            <Button
                color="primary"
                variant="contained"
            >
              Добавить новость
            </Button>
          </Link>
        </Grid>
        {newsBlock}
      </Grid>
  );
};

export default News;