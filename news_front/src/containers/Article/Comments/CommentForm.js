import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import makeStyles from "@material-ui/core/styles/makeStyles";


import axiosApi from "../../../axiosApi";
import {fetchComments} from "../../../store/actions/articleActions";


const useStyles = makeStyles({
  paper: {
    padding: "15px"
  },
  header: {
    marginBottom: "20px"
  }
});

const initialCommentState = {
  author: "",
  comment: ""
};

const CommentForm = ({id}) => {
  const classes = useStyles();

  const [comment, setComment] = useState(initialCommentState);
  const dispatch = useDispatch();

  const onChange = event => {
    const name = event.target.name;
    const value = event.target.value;

    setComment( prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const onSubmit = async event => {
    event.preventDefault();

    const commentData = {...comment, news_id: id}
    await axiosApi.post("/comments", commentData);

    setComment(initialCommentState);
    dispatch(fetchComments(id));
  };

  return (
      <Paper className={classes.paper} variant="outlined">
        <Typography className={classes.header} variant="h4">Add comment</Typography>
        <form onSubmit={onSubmit}>
          <Grid container direction="column" spacing={2}>
            <Grid item>
              <TextField
                  fullWidth
                  size="small"
                  id="author"
                  name="author"
                  label="Name"
                  variant="outlined"
                  value={comment.author}
                  onChange={onChange}
              />
            </Grid>
            <Grid item>
              <TextField
                  fullWidth
                  size="small"
                  id="comment"
                  name="comment"
                  label="Comment"
                  variant="outlined"
                  value={comment.comment}
                  onChange={onChange}
              />
            </Grid>
            <Grid item>
              <Button
                  color="primary"
                  type="submit"
                  variant="contained"
              >
                Add
              </Button>
            </Grid>
          </Grid>
        </form>
      </Paper>
  );
};

export default CommentForm;