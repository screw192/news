import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import makeStyles from "@material-ui/core/styles/makeStyles";


import {fetchComments} from "../../../store/actions/articleActions";
import CommentForm from "./CommentForm";
import CommentItem from "./Comment";


const useStyles = makeStyles({
  paper: {
    padding: "15px"
  },
  header: {
    marginBottom: "20px"
  }
});

const Comments = ({id}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const commentsArray = useSelector(state => state.article.comments);

  useEffect(() => {
    dispatch(fetchComments(id));
  }, [dispatch, id]);

  let commentsBlock = null;

  if (commentsArray.length === 0) {
    commentsBlock = (
        <Grid item>
          <Typography variant="h5">No comments here yet...</Typography>
        </Grid>
    );
  } else {
    commentsBlock = commentsArray.map(item => {
      return (
          <CommentItem
              key={"commentID" + item.id}
              id={item.id}
              newsID={item.news_id}
              author={item.author}
              comment={item.comment}
          />
      );
    });
  }

  return (
      <>
        <Grid item>
          <Paper className={classes.paper} variant="outlined">
            <Typography className={classes.header} variant="h4">Comments</Typography>
            <Grid container direction="column" spacing={1}>
              {commentsBlock}
            </Grid>
          </Paper>
        </Grid>
        <Grid item>
          <CommentForm
              id={id}
          />
        </Grid>
      </>
  );
};

export default Comments;