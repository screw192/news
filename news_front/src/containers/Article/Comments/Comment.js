import React from 'react';
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import makeStyles from "@material-ui/core/styles/makeStyles";

import axiosApi from "../../../axiosApi";
import {useDispatch} from "react-redux";
import {fetchComments} from "../../../store/actions/articleActions";


const useStyles = makeStyles({
  paper: {
    padding: "5px"
  }
});

const CommentItem = ({id, newsID, author, comment}) => {
  const classes = useStyles();

  const dispatch = useDispatch();

  const onClickHandler = async commentID => {
    await axiosApi.delete("/comments/" + commentID);
    dispatch(fetchComments(newsID));
  };

  return (
      <Grid item>
        <Paper className={classes.paper} variant="outlined">
          <Grid container alignItems="center">
            <Grid item xs>
              <Typography variant="body1"><b>{author}: </b>{comment}</Typography>
            </Grid>
            <Grid item>
              <Button
                  color="secondary"
                  variant="contained"
                  onClick={() => onClickHandler(id)}
              >
                Delete
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
  );
};

export default CommentItem;