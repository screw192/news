import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import makeStyles from "@material-ui/core/styles/makeStyles";
import moment from "moment";

import {fetchArticle} from "../../store/actions/articleActions";
import Comments from "./Comments/Comments";


const useStyles = makeStyles({
  paper: {
    padding: "15px"
  },
  timestamp: {
    marginBottom: "20px",
    color: "#999999"
  }
});

const Article = props => {
  const classes = useStyles();

  const articleID = props.match.params.id;
  const dispatch = useDispatch();
  const articleData = useSelector(state => state.article.article);

  useEffect(() => {
    dispatch(fetchArticle(articleID));
  }, [dispatch, articleID]);

  const datetime = moment(articleData.datetime).format("DD/MM/YY hh:mm:ss");

  return (
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Paper className={classes.paper} variant="outlined">
            <Typography
                variant="h4"
            >
              {articleData.title}
            </Typography>
            <Typography
                className={classes.timestamp}
                variant="body2"
            >
              {datetime}
            </Typography>
            <Typography
                variant="body1"
            >
              {articleData.article}
            </Typography>
          </Paper>
        </Grid>
        <Comments
            id={articleID}
        />
      </Grid>
  );
};

export default Article;