import React from "react";
import {Switch, Route} from "react-router-dom";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";

import News from "./containers/News/News";
import Article from "./containers/Article/Article";
import NewPostForm from "./components/NewPostForm/NewPostForm";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";


const App = () => {
  return (
      <>
        <CssBaseline/>
        <header>
          <AppToolbar/>
        </header>
        <main>
          <Container maxWidth="md">
            <Switch>
              <Route path="/" exact component={News} />
              <Route path="/news/:id" exact component={Article} />
              <Route path="/add_post" exact component={NewPostForm} />
            </Switch>
          </Container>
        </main>
      </>
  );
}

export default App;
