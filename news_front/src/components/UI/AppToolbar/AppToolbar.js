import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Link} from "react-router-dom";


const useStyles = makeStyles({
  toolbar: {
    marginBottom: "25px",
    paddingTop: "15px",
    paddingBottom: "15px"
  },
  link: {
    fontWeight: "bold",
    color: "#ffffff",
    textDecoration: "none"
  }
});

const AppToolbar = () => {
  const classes = useStyles();

  return (
      <AppBar className={classes.toolbar} position="static">
        <Container maxWidth="lg">
          <Toolbar variant="dense">
            <Typography variant="h4" color="inherit">
              <Link className={classes.link} to="/">Новости бесплатно, без регистрации и СМС</Link>
            </Typography>
          </Toolbar>
        </Container>
      </AppBar>
  );
};

export default AppToolbar;