import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";


import FileInput from "../UI/Form/FileInput";
import axiosApi from "../../axiosApi";


const useStyles = makeStyles({
  header: {
    marginBottom: "20px"
  }
});

const NewPostForm = props => {
  const classes = useStyles();

  const [newPost, setNewPost] = useState({
    title: "",
    article: "",
    image: "",
  });

  const onInputChange = event => {
    const name = event.target.name;
    const value = event.target.value;

    setNewPost( prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const onFileChange = event => {
    const name = event.target.name;
    const file = event.target.files[0];

    setNewPost( prevState => ({
      ...prevState,
      [name]: file
    }));
  };

  const onSubmitForm = async event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(newPost).forEach(key => {
      formData.append(key, newPost[key]);
    })

    await axiosApi.post("/news", formData);
    props.history.push("/");
  };

  return (
      <>
        <Typography className={classes.header} variant="h4">Add new post</Typography>
        <form onSubmit={onSubmitForm}>
          <Grid container direction="column" spacing={2}>
            <Grid item>
              <TextField
                  fullWidth
                  size="small"
                  variant="outlined"
                  id="title"
                  label="Title"
                  name="title"
                  value={newPost.title}
                  onChange={onInputChange}
              />
            </Grid>
            <Grid item>
              <TextField
                  fullWidth
                  size="small"
                  variant="outlined"
                  id="article"
                  label="Content"
                  name="article"
                  value={newPost.article}
                  onChange={onInputChange}
              />
            </Grid>
            <Grid item>
              <FileInput
                  name="image"
                  label="Select image"
                  onChange={onFileChange}
              />
            </Grid>
            <Grid item>
              <Button
                  color="primary"
                  variant="contained"
                  type="submit"
              >
                Save
              </Button>
            </Grid>
          </Grid>
        </form>
      </>

  );
};

export default NewPostForm;