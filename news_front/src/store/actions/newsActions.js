import axiosApi from "../../axiosApi";

export const FETCH_NEWS_REQUEST = "FETCH_NEWS_REQUEST";
export const FETCH_NEWS_SUCCESS = "FETCH_NEWS_SUCCESS";
export const FETCH_NEWS_FAILURE = "FETCH_NEWS_FAILURE";

export const fetchNewsRequest = () => ({type: FETCH_NEWS_REQUEST});
export const fetchNewsSuccess = newsData => ({type: FETCH_NEWS_SUCCESS, newsData});
export const fetchNewsFailure = () => ({type: FETCH_NEWS_FAILURE});

export const fetchNews = () => {
  return async dispatch => {
    try {
      dispatch(fetchNewsRequest());

      const result = await axiosApi.get("/news");
      dispatch(fetchNewsSuccess(result.data));
    } catch (e) {
      dispatch(fetchNewsFailure());
    }
  };
};