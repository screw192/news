import axiosApi from "../../axiosApi";

export const INIT_ARTICLE_COMMENTS = "INIT_ARTICLE_COMMENTS";

export const FETCH_ARTICLE_REQUEST = "FETCH_ARTICLE_REQUEST";
export const FETCH_ARTICLE_SUCCESS = "FETCH_ARTICLE_SUCCESS";
export const FETCH_ARTICLE_FAILURE = "FETCH_ARTICLE_FAILURE";

export const FETCH_COMMENTS_REQUEST = "FETCH_COMMENTS_REQUEST";
export const FETCH_COMMENTS_SUCCESS = "FETCH_COMMENTS_SUCCESS";
export const FETCH_COMMENTS_FAILURE = "FETCH_COMMENTS_FAILURE";

export const initArticleComments = () => ({type: INIT_ARTICLE_COMMENTS});

export const fetchArticleRequest = () => ({type: FETCH_ARTICLE_REQUEST});
export const fetchArticleSuccess = articleData => ({type: FETCH_ARTICLE_SUCCESS, articleData});
export const fetchArticleFailure = () => ({type: FETCH_ARTICLE_FAILURE});

export const fetchArticle = id => {
  return async dispatch => {
    try {
      dispatch(fetchArticleRequest());

      const result = await axiosApi.get("/news/" + id);
      dispatch(fetchArticleSuccess(result.data));
    } catch (e) {
      dispatch(fetchArticleFailure());
    }
  };
};

export const fetchCommentsRequest = () => ({type: FETCH_COMMENTS_REQUEST});
export const fetchCommentsSuccess = commentsData => ({type: FETCH_COMMENTS_SUCCESS, commentsData});
export const fetchCommentsFailure = () => ({type: FETCH_COMMENTS_FAILURE});

export const fetchComments = id => {
  return async dispatch => {
    try {
      dispatch(fetchCommentsRequest());

      const result = await axiosApi.get("/comments?news_id=" + id);
      dispatch(fetchCommentsSuccess(result.data));
    } catch (e) {
      dispatch(fetchCommentsFailure());
    }
  };
};