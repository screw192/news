import {FETCH_NEWS_FAILURE, FETCH_NEWS_REQUEST, FETCH_NEWS_SUCCESS} from "../actions/newsActions";

const initialState = {
  news: [],
  newsLoading: true,
};

const newsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_NEWS_REQUEST:
      return {...state, newsLoading: true};
    case FETCH_NEWS_SUCCESS:
      return {...state, newsLoading: false, news: action.newsData};
    case FETCH_NEWS_FAILURE:
      return {...state, newsLoading: false};
    default:
      return state;
  }
};

export default newsReducer;