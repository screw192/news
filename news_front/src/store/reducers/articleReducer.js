import {
  FETCH_ARTICLE_FAILURE,
  FETCH_ARTICLE_REQUEST,
  FETCH_ARTICLE_SUCCESS,
  FETCH_COMMENTS_FAILURE,
  FETCH_COMMENTS_REQUEST,
  FETCH_COMMENTS_SUCCESS, INIT_ARTICLE_COMMENTS
} from "../actions/articleActions";

const initialState = {
  article: [],
  articleLoading: true,
  comments: [],
  commentsLoading: false,
};

const articleReducer = (state = initialState, action) => {
  switch (action.type) {
    case INIT_ARTICLE_COMMENTS:
      return initialState;
    case FETCH_ARTICLE_REQUEST:
      return {...state, articleLoading: true};
    case FETCH_ARTICLE_SUCCESS:
      return {...state, articleLoading: false, article: action.articleData};
    case FETCH_ARTICLE_FAILURE:
      return {...state, articleLoading: false};
    case FETCH_COMMENTS_REQUEST:
      return {...state, commentsLoading: true};
    case FETCH_COMMENTS_SUCCESS:
      return {...state, commentsLoading: false, comments: action.commentsData};
    case FETCH_COMMENTS_FAILURE:
      return {...state, commentsLoading: false};
    default:
      return state;
  }
};

export default articleReducer;