const express = require("express");
const cors = require("cors");
const mysqlDB = require("./mysqlDB");
const news = require("./app/news");
const comments = require("./app/comments");


const app = express();
app.use(express.static("public"));
app.use(express.json());
app.use(cors());

const port = 8000;

app.use("/news", news);
app.use("/comments", comments);

const run = async () => {
  await mysqlDB.connect();

  app.listen(port, () => {
    console.log(`Server started on ${port} port`);
  });
};

run().catch(console.error);