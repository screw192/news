const express = require("express");
const mysqlDB = require("../mysqlDB");
const multer = require("multer");
const {nanoid} = require("nanoid");
const config = require("../config");
const path = require("path");


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({storage});

const router = express.Router();

router.get("/", async (req, res) => {
  const reqTargetFields = ["id", "title", "image",  "datetime"];
  const [news] = await mysqlDB.getConnection().query(
      "SELECT ?? FROM news",
      [reqTargetFields]
  );

  res.send(news);
});

router.get("/:id", async (req, res) => {
  const [result] = await mysqlDB.getConnection().query(
      "SELECT * FROM news WHERE id = ?",
      [req.params.id]
  );

  const newsItem = result[0];

  res.send(newsItem);
});

router.post("/", upload.single("image"), async (req, res) => {
  const newsItemData = req.body;

  if (req.file) {
    newsItemData.image = req.file.filename;
  } else {
    newsItemData.image = null;
  }

  const [result] = await mysqlDB.getConnection().query(
      "INSERT INTO news SET ?",
      newsItemData
  );

  res.send({...newsItemData, id: result.insertId});
});

router.delete("/:id", async (req, res) => {
  const [result] = await mysqlDB.getConnection().query(
      "DELETE FROM news WHERE id = ?",
      [req.params.id]
  );

  res.send(result);
});

module.exports = router;