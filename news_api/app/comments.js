const express = require("express");
const mysqlDB = require("../mysqlDB");


const router = express.Router();

router.get("/", async (req, res) => {
  if (req.query.news_id) {
    const [news] = await mysqlDB.getConnection().query(
        "SELECT * FROM comments WHERE news_id = ?",
        [req.query.news_id]
    );

    res.send(news);
  } else {
    const [news] = await mysqlDB.getConnection().query(
        "SELECT * FROM comments"
    );

    res.send(news);
  }
});

router.post("/", async (req, res) => {
  const commentData = req.body;

  if (commentData.author === "") {
    commentData.author = "Anonymous";
  }

  const [news] = await mysqlDB.getConnection().query(
      "INSERT INTO comments SET ?",
      commentData
  );

  res.send({...commentData, id: news.insertId});
});

router.delete("/:id", async (req, res) => {
  const [result] = await mysqlDB.getConnection().query(
      "DELETE FROM comments WHERE id = ?",
      [req.params.id]
  );

  res.send(result);
});

module.exports = router;